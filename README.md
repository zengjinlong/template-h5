# h5开发模板-B端

## 安装项目依赖包
```
yarn install
```

### 运行项目
```
请把.env.local文件里的VUE_APP_API改为本地环境的接口地址
yarn run serve
```
 
### 生产环境项目打包
```
请把.env.prod文件里的VUE_APP_API改为生产环境的接口地址
yarn run build:prod
```
### 测试环境项目打包
```
请把.env.test文件里的VUE_APP_API改为测试环境的接口地址
yarn run build:test
```
