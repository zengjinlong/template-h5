/**
 * @create by Jaylen
 * @Date 2019.12.17 
 */
const webpack = require('webpack')
const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin')
const path = require('path')

const dllReference = (config) => {
   config.plugin('vendorDll')
      .use(webpack.DllReferencePlugin, [{
         context: __dirname,
         manifest: require('./dll/vendor.manifest.json')
      }])

   config.plugin('utilDll')
      .use(webpack.DllReferencePlugin, [{
         context: __dirname,
         manifest: require('./dll/util.manifest.json')
      }])

   config.plugin('addAssetHtml')
      .use(AddAssetHtmlPlugin, [
         [{
               filepath: require.resolve(path.resolve(__dirname, 'dll/vendor.dll.js')),
               outputPath: 'dll',
               publicPath: 'dll'
            },
            {
               filepath: require.resolve(path.resolve(__dirname, 'dll/util.dll.js')),
               outputPath: 'dll',
               publicPath: 'dll'
            }
         ]
      ])
      .after('html')
}

module.exports = {
   publicPath: './',
   outputDir: process.env.NODE_ENV === 'test' ? 'test_dist' : 'dist',
   productionSourceMap: false,
   css: {
      // 是否使用css分离插件 ExtractTextPlugin
      extract: true,
      // 开启 CSS source maps?
      sourceMap: false,
      // css预设器配置项
      loaderOptions: {},
      // 启用 CSS modules for all css / pre-processor files.
      requireModuleExtension: false
   },
   chainWebpack: config => {
      if (process.env.NODE_ENV === 'production') {
         dllReference(config)
      }
   },
   configureWebpack: config => {
      if (process.env.NODE_ENV === 'production') {
         config.optimization.minimizer[0].options.terserOptions.compress.warnings = false
         config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true
         config.optimization.minimizer[0].options.terserOptions.compress.drop_debugger = true
         config.optimization.minimizer[0].options.terserOptions.compress.pure_funcs = ['console.log']
      }
      config.optimization.splitChunks = {
         cacheGroups: {
            vendor: {
               chunks: "all",
               test: /node_modules/,
               name: "vendor",
               minChunks: 1,
               maxInitialRequests: 5,
               minSize: 0,
               priority: 100,
            },
            common: {
               chunks: "all",
               test: /[\\/]src[\\/]js[\\/]/,
               name: "common",
               minChunks: 2,
               maxInitialRequests: 5,
               minSize: 0,
               priority: 60
            },
            styles: {
               name: 'styles',
               test: /\.(sa|sc|c)ss$/,
               chunks: 'all',
               enforce: true,
            },
            runtimeChunk: {
               name: 'manifest'
            }
         }
      }
   },
}