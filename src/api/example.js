import httpRequest from '@/utils/axios'
/**
 * @param {axios's option} httpRequest 
 */
export const example = (data) => {
    return httpRequest({
        url: `/api/getSomething`,
        method: 'GET',
        params: data
    })
}